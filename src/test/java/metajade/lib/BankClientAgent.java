/********************************************************************************
 * BankClientAgent:
 * ---------------
 * This class is an example showing how to use the classes OneShotBehaviour,
 * SimpleBehaviour, WakerBehaviour and ParallelBehaviour to program an agent.
 * <p>
 * Version 1.0 - July 2003
 * Author: Ambroise Ncho, under the supervision of Professor Jean Vaucher
 * <p>
 * University of Montreal - DIRO
 *
 * Last modified by Riccardo Battistini, December 2022
 *
 *********************************************************************************/

package metajade.lib;

import jade.content.AgentAction;
import jade.content.ContentElement;
import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.basic.Action;
import jade.content.onto.basic.Result;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.util.leap.Iterator;
import jade.util.leap.List;
import metajade.examples.bank.ontology.*;
import metajade.examples.bank.utils.Command;

import static metajade.examples.bank.utils.BankUtils.SERVER_AGENT;

public class BankClientAgent extends TestingAgent {
    private String chosenAccountName = "john";
    private String chosenAccountAmount = "230";
    private AID server;
    private final Codec codec = new SLCodec();
    private final Ontology ontology = BankOntology.getInstance();
    private String dummyAccountId = "1";

    @Override
    public Behaviour mainBehaviour() {
        Object[] args = getArguments();
        String cmd;

        if(args[0] != null) {
           cmd = args[0].toString();
        } else {
           cmd = "1";
        }
        log("Received command `%s`", cmd);

        if(args[1] != null) {
            chosenAccountName = args[1].toString();
        } else {
            chosenAccountName = "john";
        }
        log("Received account name `%s`", chosenAccountName);

        if(args[2] != null) {
            chosenAccountAmount = args[2].toString();
        } else {
            chosenAccountAmount = "230";
        }
        log("Received account amount `%s`", chosenAccountAmount);

        if(args[3] != null) {
            dummyAccountId = args[3].toString();
        } else {
            dummyAccountId = "1";
        }
        log("Received account id `%s`", dummyAccountId);

        getContentManager().registerLanguage(codec);
        getContentManager().registerOntology(ontology);

        return Behaviours.atomic(() -> addBehaviour(new HandleCommand(this, parseCommand(cmd))));
    }

    private Command parseCommand(String cmd) {
        int cmdInt = Integer.parseInt(cmd);
        return Command.fromValue(cmdInt);
    }

    class HandleCommand extends OneShotBehaviour {
        private final Command command;
        HandleCommand(Agent a, Command command) {
            super(a);
            this.command = command;
        }

        public void action() {
            switch (command) {
                case balance: case operations:
                    if(!dummyAccountId.equals("")) {
                        requestInformation(command);
                    } else {
                        log("Account Id not set");
                    }
                    break;
                case deposit: case withdraw:
                    if(!dummyAccountId.equals("")) {
                        requestOperation(command);
                    } else {
                        log("Account Id not set");
                    }
                    break;
                case new_account:
                    requestAccountCreation();
                    break;
                default:
                    throw new IllegalArgumentException("Unexpected command '" + command + "'");
            }
        }
    }

    void requestAccountCreation() {
        CreateAccount ca = new CreateAccount();
        ca.setName(chosenAccountName);

        log("Requesting account creation (name: %s)`", chosenAccountName);
        addBehaviour(Behaviours.sequence(
                Behaviours.sendMessage(composeMessage(ACLMessage.REQUEST, ca)),
                Behaviours.receiveMessage(
                        MessageTemplate.MatchSender(server),
                        this::handleResponse
                )
        ));
    }

    void requestOperation(Command command) {
        float amount;
        amount = Float.parseFloat(chosenAccountAmount);
        MakeOperation mo = new MakeOperation();
        mo.setType(command.getValue());
        mo.setAmount(amount);
        mo.setAccountId(dummyAccountId);

        if(command.getValue() == Command.deposit.getValue()) {
            log("Requesting deposit (id: %s, amount: %s)`", dummyAccountId, chosenAccountAmount);
        } else if(command.getValue() == Command.withdraw.getValue()) {
            log("Requesting withdraw (id: %s, amount: %s)`", dummyAccountId, chosenAccountAmount);
        }
        addBehaviour(Behaviours.sequence(
                Behaviours.sendMessage(composeMessage(ACLMessage.REQUEST, mo)),
                Behaviours.receiveMessage(
                        MessageTemplate.MatchSender(server),
                        this::handleResponse
                )
        ));
    }

    void requestInformation(Command command) {
        Information info = new Information();
        info.setType(command.getValue());
        info.setAccountId(dummyAccountId);

        if(command.getValue() == Command.balance.getValue()) {
            log("Requesting current balance (id: %s)`", dummyAccountId);
        } else if(command.getValue() == Command.operations.getValue()) {
            log("Requesting list of operations (id: %s)`", dummyAccountId);
        }
        addBehaviour(Behaviours.sequence(
                Behaviours.sendMessage(composeMessage(ACLMessage.QUERY_REF, info)),
                Behaviours.receiveMessage(
                        MessageTemplate.MatchSender(server),
                        this::handleResponse
                )
        ));
    }

    private void handleResponse(ACLMessage msg) {
        try {
            ContentElement content = getContentManager().extractContent(msg);
            if (content instanceof Result) {
                Result result = (Result) content;
                if (result.getValue() instanceof Problem) {
                    Problem prob = (Problem)result.getValue();
                    log("Received Problem %s", prob.getMsg());
                } else if (result.getValue() instanceof Account) {
                    Account acc = (Account) result.getValue();
                    log("Received Account %s with balance %s", acc.getName(), acc.getBalance());
                } else if (result.getValue() instanceof List) {
                    List list = result.getItems();
                    StringBuilder s = new StringBuilder();
                    for (Iterator it = list.iterator(); it.hasNext();) {
                        s.append(it.next().toString());
                        s.append(" ");
                    }
                    log("%s", s);
                } else {
                    log("Unexpected result from server");
                }
            } else {
                log("Unable de decode response from server");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void lookupServer() {
        ServiceDescription sd = new ServiceDescription();
        sd.setType(SERVER_AGENT);
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.addServices(sd);
        try {
            DFAgentDescription[] dfds = DFService.search(this, dfd);
            if (dfds.length > 0 ) {
                server = dfds[0].getName();
                log("Localized server");
            }
            else {
                log("Couldn't localize server!");
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
            log("Failed searching int the DF!");
        }
    }

    private ACLMessage composeMessage(int performative, AgentAction action) {
        if (server == null) lookupServer();
        if (server == null) {
            log("Unable to localize the server");
            return null;
        }
        ACLMessage msg = new ACLMessage(performative);
        msg.setLanguage(codec.getName());
        msg.setOntology(ontology.getName());
        try {
            getContentManager().fillContent(msg, new Action(server, action));
            msg.addReceiver(server);
            return msg;
        }
        catch (Exception ex) { ex.printStackTrace(); }
        return null;
    }

}
