package metajade.examples.bank;

import metajade.lib.BankClientAgent;
import metajade.lib.JadeTestingUtils;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import java.util.Arrays;
import java.util.Random;

import static metajade.lib.JadeTestingUtils.*;

public class TestBank {
    @BeforeClass
    public static void startPlatform() {
        JadeTestingUtils.startNewPlatform();
    }

    @Test
    public void testBankOperations() throws InterruptedException {
        final Random random = new Random();
        var bankServerName = "server_" + random.nextLong();

        spawnAgent(bankServerName, BankServerAgent.class);

        Thread.sleep(200);

        Object[] args = new Object[4];
        args[0] = "1";
        args[1] = "john";
        args[2] = "230";
        args[3] = "1";
        var bankClientName = "client_" + random.nextLong();
        var bankClient = spawnTestAgent(bankClientName, BankClientAgent.class, args);
        bankClient.awaitTermination();

        Assert.assertEquals(
                Arrays.asList(
                        "Received command `1`",
                        "Received account name `john`",
                        "Received account amount `230`",
                        "Received account id `1`",
                        "Requesting account creation (name: john)`",
                        "Localized server",
                        "Received Account john with balance 0.0"
                ), bankClient.getLogs()
        );

        args[0] = "2";
        bankClientName = "client_" + random.nextLong();
        bankClient = spawnTestAgent(bankClientName, BankClientAgent.class, args);
        bankClient.awaitTermination();

        Assert.assertEquals(
                Arrays.asList(
                        "Received command `2`",
                        "Received account name `john`",
                        "Received account amount `230`",
                        "Received account id `1`",
                        "Requesting deposit (id: 1, amount: 230)`",
                        "Localized server",
                        "Received Account john with balance 230.0"
                ), bankClient.getLogs()
        );

        args[0] = "3";
        bankClientName = "client_" + random.nextLong();
        bankClient = spawnTestAgent(bankClientName, BankClientAgent.class, args);
        bankClient.awaitTermination();

        Assert.assertEquals(
                Arrays.asList(
                        "Received command `3`",
                        "Received account name `john`",
                        "Received account amount `230`",
                        "Received account id `1`",
                        "Requesting current balance (id: 1)`",
                        "Localized server",
                        "Received Account john with balance 230.0"
                ), bankClient.getLogs()
        );

        args[0] = "4";
        bankClientName = "client_" + random.nextLong();
        bankClient = spawnTestAgent(bankClientName, BankClientAgent.class, args);
        bankClient.awaitTermination();

        Assert.assertEquals(
                Arrays.asList(
                        "Received command `4`",
                        "Received account name `john`",
                        "Received account amount `230`",
                        "Received account id `1`",
                        "Requesting withdraw (id: 1, amount: 230)`",
                        "Localized server",
                        "Received Account john with balance 0.0"
                ), bankClient.getLogs()
        );

        args[0] = "5";
        bankClientName = "client_" + random.nextLong();
        bankClient = spawnTestAgent(bankClientName, BankClientAgent.class, args);
        bankClient.awaitTermination();

        Assert.assertEquals(
                Arrays.asList(
                        "Received command `5`",
                        "Received account name `john`",
                        "Received account amount `230`",
                        "Received account id `1`",
                        "Requesting list of operations (id: 1)`",
                        "Localized server",
                        "Depos.  230.0  0.0 Withd.  230.0  0.0 "
                ), bankClient.getLogs()
        );

    }

    @Test
    public void testInsufficientFunds() throws InterruptedException {
        final Random random = new Random();
        var bankServerName = "server_" + random.nextLong();

        spawnAgent(bankServerName, BankServerAgent.class);

        Thread.sleep(200);

        Object[] args = new Object[4];
        args[0] = "1";
        args[1] = "john";
        args[2] = "230";
        args[3] = "1";
        var bankClientName = "client_" + random.nextLong();
        var bankClient = spawnTestAgent(bankClientName, BankClientAgent.class, args);
        bankClient.awaitTermination();

        args[0] = "2";
        bankClientName = "client_" + random.nextLong();
        bankClient = spawnTestAgent(bankClientName, BankClientAgent.class, args);
        bankClient.awaitTermination();

        args[0] = "4";
        args[2] = "430";
        bankClientName = "client_" + random.nextLong();
        bankClient = spawnTestAgent(bankClientName, BankClientAgent.class, args);
        bankClient.awaitTermination();

        Assert.assertEquals(
                Arrays.asList(
                        "Received command `4`",
                        "Received account name `john`",
                        "Received account amount `430`",
                        "Received account id `1`",
                        "Requesting withdraw (id: 1, amount: 430)`",
                        "Localized server",
                        "Received Problem Insufficient Funds"
                ), bankClient.getLogs()
        );
    }

    @Test
    public void testInvalidAmount() throws InterruptedException {
        final Random random = new Random();
        var bankServerName = "server_" + random.nextLong();

        spawnAgent(bankServerName, BankServerAgent.class);

        Thread.sleep(200);

        Object[] args = new Object[4];
        args[0] = "1";
        args[1] = "john";
        args[2] = "230";
        args[3] = "1";
        var bankClientName = "client_" + random.nextLong();
        var bankClient = spawnTestAgent(bankClientName, BankClientAgent.class, args);
        bankClient.awaitTermination();

        args[0] = "2";
        args[2] = "-230";
        bankClientName = "client_" + random.nextLong();
        bankClient = spawnTestAgent(bankClientName, BankClientAgent.class, args);
        bankClient.awaitTermination();

        Assert.assertEquals(
                Arrays.asList(
                        "Received command `2`",
                        "Received account name `john`",
                        "Received account amount `-230`",
                        "Received account id `1`",
                        "Requesting deposit (id: 1, amount: -230)`",
                        "Localized server",
                        "Received Problem Invalid amount"
                ), bankClient.getLogs()
        );
    }

    @Test
    public void testInvalidAccount() throws InterruptedException {
        final Random random = new Random();
        var bankServerName = "server_" + random.nextLong();

        spawnAgent(bankServerName, BankServerAgent.class);

        Thread.sleep(200);

        Object[] args = new Object[4];
        args[0] = "1";
        args[1] = "john";
        args[2] = "230";
        args[3] = "1";
        var bankClientName = "client_" + random.nextLong();
        var bankClient = spawnTestAgent(bankClientName, BankClientAgent.class, args);
        bankClient.awaitTermination();

        args[0] = "2";
        args[3] = "2";
        bankClientName = "client_" + random.nextLong();
        bankClient = spawnTestAgent(bankClientName, BankClientAgent.class, args);
        bankClient.awaitTermination();

        Assert.assertEquals(
                Arrays.asList(
                        "Received command `2`",
                        "Received account name `john`",
                        "Received account amount `230`",
                        "Received account id `2`",
                        "Requesting deposit (id: 2, amount: 230)`",
                        "Localized server",
                        "Received Problem Account not found"
                ), bankClient.getLogs()
        );
    }

    @AfterClass
    public static void stopPlatform() {
        JadeTestingUtils.stopPlatform();
    }
}
