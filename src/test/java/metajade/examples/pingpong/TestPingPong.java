package metajade.examples.pingpong;

import metajade.lib.JadeTestingUtils;
import metajade.lib.Ponger;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static metajade.lib.JadeTestingUtils.spawnAgent;
import static metajade.lib.JadeTestingUtils.spawnTestAgent;

public class TestPingPong {
    @BeforeClass
    public static void startPlatform() {
        JadeTestingUtils.startNewPlatform();
    }

    @Test
    public void testPingPong() throws InterruptedException {
        final Random random = new Random();
        var pongerName = "ponger_" + random.nextLong();
        var ponger = spawnTestAgent(pongerName, Ponger.class);

        var pingerName = "pinger_" + random.nextLong();
        spawnAgent(pingerName, Pinger.class);

        ponger.awaitTermination();

        Assert.assertEquals(
                IntStream.range(0, 2 * Ponger.ROUNDS + 1)
                        .mapToObj(i -> {
                            if (i % 2 == 0) {
                                return "Received `ping` from " + pingerName;
                            } else {
                                return "Sent `pong` to " + pingerName;
                            }
                        }).collect(Collectors.toList()),
                ponger.getLogs()
        );
    }

    @AfterClass
    public static void stopPlatform() {
        JadeTestingUtils.stopPlatform();
    }
}
