/********************************************************************************
 * BankServerAgent:
 * ---------------
 * This class is an example showing how to use the classes SequentialBehaviour,
 * OneShotBehaviour and CyclicBehaviour to build an agent.
 * <p>
 * Version 1.0 - July 2003
 * Author: Ambroise Ncho, under the supervision of Professor Jean Vaucher.
 * <p>
 * University of Montreal - DIRO
 *
 * Last modified by Riccardo Battistini, December 2022
 *
 *********************************************************************************/

package metajade.examples.bank;

import jade.content.Concept;
import jade.content.ContentElement;
import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.basic.Action;
import jade.content.onto.basic.Result;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SequentialBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.util.leap.ArrayList;
import jade.util.leap.HashMap;
import jade.util.leap.List;
import jade.util.leap.Map;
import metajade.examples.bank.ontology.*;
import metajade.examples.bank.utils.Command;
import metajade.examples.bank.utils.ErrorCodes;

import static metajade.examples.bank.utils.BankUtils.SERVER_AGENT;

public class BankServerAgent extends Agent implements BankVocabulary {
    private final Map accounts = new HashMap();
    private final Map operations = new HashMap();
    private final Codec codec = new SLCodec();
    private final Ontology ontology = BankOntology.getInstance();

    protected void setup() {
        getContentManager().registerLanguage(codec);
        getContentManager().registerOntology(ontology);

        SequentialBehaviour sb = new SequentialBehaviour();
        sb.addSubBehaviour(new RegisterInDF(this));
        sb.addSubBehaviour(new ReceiveMessages(this));
        addBehaviour(sb);
    }

    class RegisterInDF extends OneShotBehaviour {
        RegisterInDF(Agent a) {
            super(a);
        }

        public void action() {
            ServiceDescription sd = new ServiceDescription();
            sd.setType(SERVER_AGENT);
            sd.setName(getName());
            DFAgentDescription dfd = new DFAgentDescription();
            dfd.setName(getAID());
            dfd.addServices(sd);
            try {
                DFAgentDescription[] dfds = DFService.search(myAgent, dfd);
                if (dfds.length > 0 ) {
                    DFService.deregister(myAgent, dfd);
                }
                DFService.register(myAgent, dfd);
                log("Server registered");
            }
            catch (Exception ex) {
                log("Failed registering with DF! Shutting down...");
                ex.printStackTrace();
                doDelete();
            }
        }
    }

    class ReceiveMessages extends CyclicBehaviour {
        public ReceiveMessages(Agent a) {
            super(a);
        }

        public void action() {
            ACLMessage msg = receive();
            if (msg == null) {
                block(); return;
            }
            try {
                ContentElement content = getContentManager().extractContent(msg);
                Concept action = ((Action)content).getAction();

                switch (msg.getPerformative()) {
                    case (ACLMessage.REQUEST):
                        log("Request received");
                        if (action instanceof CreateAccount) {
                            addBehaviour(new HandleCreateAccount(myAgent, msg));
                        } else if (action instanceof MakeOperation) {
                            addBehaviour(new HandleOperation(myAgent, msg));
                        } else {
                            replyNotUnderstood(msg);
                        }
                        break;

                    case (ACLMessage.QUERY_REF):
                        log("Query from %s", msg.getSender().getLocalName());
                        if (action instanceof Information) {
                            addBehaviour(new HandleInformation(myAgent, msg));
                        } else {
                            replyNotUnderstood(msg);
                        }
                        break;

                    default: replyNotUnderstood(msg);
                }
            }
            catch(Exception ex) { ex.printStackTrace(); }
        }
    }

    class HandleCreateAccount extends OneShotBehaviour {
        private final ACLMessage request;

        HandleCreateAccount(Agent a, ACLMessage request) {
            super(a);
            this.request = request;
        }

        public void action() {
            try {
                ContentElement content = getContentManager().extractContent(request);
                CreateAccount ca = (CreateAccount)((Action)content).getAction();
                Account acc = new Account();
                String id = generateId();
                acc.setId(id);
                acc.setName(ca.getName());
                Result result = new Result((Action)content, acc);
                ACLMessage reply = request.createReply();
                reply.setPerformative(ACLMessage.INFORM);
                getContentManager().fillContent(reply, result);
                send(reply);
                accounts.put(id, acc);
                operations.put(id, new ArrayList());
                log("Created Account (name: %s, id: %s)", acc.getName(), acc.getId());
            }
            catch(Exception ex) { ex.printStackTrace(); }
        }
    }

    class HandleOperation extends OneShotBehaviour {
        private final ACLMessage request;

        HandleOperation(Agent a, ACLMessage request) {
            super(a);
            this.request = request;
        }

        public void action() {
            try {
                ContentElement content = getContentManager().extractContent(request);
                MakeOperation mo = (MakeOperation)((Action)content).getAction();
                ACLMessage reply = request.createReply();
                Object obj = processOperation(mo);
                if (obj == null) replyNotUnderstood(request);
                else {
                    reply.setPerformative(ACLMessage.INFORM);
                    Result result = new Result((Action)content, obj);
                    getContentManager().fillContent(reply, result);
                    send(reply);
                }
            }
            catch(Exception ex) { ex.printStackTrace(); }
        }
    }

    class HandleInformation extends OneShotBehaviour {
        private final ACLMessage query;

        HandleInformation(Agent a, ACLMessage query) {
            super(a);
            this.query = query;
        }

        public void action() {
            try {
                ContentElement content = getContentManager().extractContent(query);
                Information info = (Information)((Action)content).getAction();
                Object obj = processInformation(info);
                if (obj == null) {
                    replyNotUnderstood(query);
                } else {
                    ACLMessage reply = query.createReply();
                    reply.setPerformative(ACLMessage.INFORM);
                    Result result = new Result((Action)content, obj);
                    getContentManager().fillContent(reply, result);
                    send(reply);
                }
            }
            catch(Exception ex) { ex.printStackTrace(); }
        }
    }

    void replyNotUnderstood(ACLMessage msg) {
        try {
            ContentElement content = getContentManager().extractContent(msg);
            ACLMessage reply = msg.createReply();
            reply.setPerformative(ACLMessage.NOT_UNDERSTOOD);
            getContentManager().fillContent(reply, content);
            send(reply);
            log("Message not understood!");
        }
        catch(Exception ex) { ex.printStackTrace(); }
    }

    Object processOperation(MakeOperation mo) {
        Account acc = (Account) accounts.get(mo.getAccountId());
        if (acc == null) {
            log("Account with id %s not found", mo.getAccountId());
            return newProblem(ErrorCodes.accountNotFound.getValue());
        }
        if (mo.getAmount() <= 0) {
            log("Invalid Amount of %s", mo.getAmount());
            return newProblem(ErrorCodes.invalidAmount.getValue());
        }

        if (mo.getType() != Command.deposit.getValue() && mo.getType() != Command.withdraw.getValue()) {
            return null;
        }

        if (mo.getType() == Command.deposit.getValue()) {
            log("Executed Deposit of %s", mo.getAmount());
            acc.setBalance(acc.getBalance() + mo.getAmount());
        } else if (mo.getType() == Command.withdraw.getValue()) {
            if (mo.getAmount() > acc.getBalance()) {
                log("Insufficient Funds");
                return newProblem(ErrorCodes.insufficientFunds.getValue());
            }
            log("Executed Withdraw of %s", mo.getAmount());
            acc.setBalance(acc.getBalance() - mo.getAmount());
        }
        Operation op = new Operation();
        op.setType(mo.getType());
        op.setAmount(mo.getAmount());
        op.setAccountId(acc.getId());
        op.setDate(new java.util.Date());
        List l = (List)operations.get(acc.getId());
        l.add(op);
        operations.put(acc.getId(), l);
        return acc;
    }

    private Object processInformation(Information info) {
        Account acc = (Account) accounts.get(info.getAccountId());
        if (acc == null) return newProblem(ErrorCodes.accountNotFound.getValue());

        List l = (List)operations.get(acc.getId());
        if (info.getType() == Command.balance.getValue()) {
            log("Balance is %s", acc.getBalance());
            return acc;
        }
        if (info.getType() == Command.operations.getValue()) {
            log("Collected list of operations");
            return l;
        }
        return null;
    }

    private String generateId() {
        return "1";
    }


    private Problem newProblem(int num) {
        String msg = "";

        if (num == ErrorCodes.accountNotFound.getValue()) {
            msg = "Account not found";
        } else if (num == ErrorCodes.insufficientFunds.getValue()) {
            msg = "Insufficient Funds";
        } else if (num == ErrorCodes.invalidAmount.getValue()) {
            msg = "Invalid amount";
        }

        Problem prob = new Problem();
        prob.setNum(num);
        prob.setMsg(msg);
        return prob;
    }

    protected final void log(final String format, Object... args) {
        System.out.println("[" + getAID().getLocalName() + "]: " + String.format(format, args));
    }
}


