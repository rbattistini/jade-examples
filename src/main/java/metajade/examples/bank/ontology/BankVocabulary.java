package metajade.examples.bank.ontology;

public interface BankVocabulary {
    String ACCOUNT = "Account";
    String ACCOUNT_ID = "id";
    String ACCOUNT_NAME = "name";
    String ACCOUNT_BALANCE = "balance";

    String CREATE_ACCOUNT = "Create_account";
    String CREATE_ACCOUNT_NAME = "name";

    String OPERATION = "Operation";
    String OPERATION_TYPE = "type";
    String OPERATION_AMOUNT = "amount";
    String OPERATION_BALANCE = "balance";
    String OPERATION_ACCOUNTID = "accountId";
    String OPERATION_DATE = "date";

    String MAKE_OPERATION = "MakeOperation";
    String MAKE_OPERATION_TYPE = "type";
    String MAKE_OPERATION_AMOUNT = "amount";
    String MAKE_OPERATION_ACCOUNTID = "accountId";

    String INFORMATION = "Information";
    String INFORMATION_TYPE = "type";
    String INFORMATION_ACCOUNTID = "accountId";

    String PROBLEM = "Problem";
    String PROBLEM_NUM = "num";
    String PROBLEM_MSG="msg";
}

