/********************************************************************************
 * BankOntology:
 * -------------
 * This class is an example showing how to use JADE content language support  ,
 * to define an application ontology.
 * <p>
 * Version 1.0 - July 2003
 * Author: Ambroise Ncho, under the supervision of Professeur Jean Vaucher.
 * <p>
 * Universite de Montreal - DIRO
 *
 * Last modified by Riccardo Battistini, December 2022
 *
 *********************************************************************************/

package metajade.examples.bank.ontology;

import jade.content.onto.BasicOntology;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.schema.AgentActionSchema;
import jade.content.schema.ConceptSchema;
import jade.content.schema.ObjectSchema;
import jade.content.schema.PrimitiveSchema;


public class BankOntology extends Ontology implements BankVocabulary {
    public static final String ONTOLOGY_NAME = "Bank-Ontology";
    private static final Ontology instance = new BankOntology();
    public static Ontology getInstance() { return instance; }

    private BankOntology() {
        super(ONTOLOGY_NAME, BasicOntology.getInstance());

        try {
            ConceptSchema cs = new ConceptSchema(ACCOUNT);
            add(cs, Account.class);
            cs.add(ACCOUNT_ID, (PrimitiveSchema) getSchema(BasicOntology.STRING), ObjectSchema.MANDATORY);
            cs.add(ACCOUNT_NAME, (PrimitiveSchema) getSchema(BasicOntology.STRING), ObjectSchema.MANDATORY);
            cs.add(ACCOUNT_BALANCE, (PrimitiveSchema) getSchema(BasicOntology.FLOAT), ObjectSchema.MANDATORY);

            add(cs = new ConceptSchema(PROBLEM), Problem.class);
            cs.add(PROBLEM_NUM, (PrimitiveSchema) getSchema(BasicOntology.INTEGER), ObjectSchema.MANDATORY);
            cs.add(PROBLEM_MSG, (PrimitiveSchema) getSchema(BasicOntology.STRING), ObjectSchema.MANDATORY);

            add(cs = new ConceptSchema(OPERATION), Operation.class);
            cs.add(OPERATION_TYPE, (PrimitiveSchema) getSchema(BasicOntology.INTEGER), ObjectSchema.MANDATORY);
            cs.add(OPERATION_AMOUNT, (PrimitiveSchema) getSchema(BasicOntology.FLOAT), ObjectSchema.MANDATORY);
            cs.add(OPERATION_BALANCE, (PrimitiveSchema) getSchema(BasicOntology.FLOAT), ObjectSchema.MANDATORY);
            cs.add(OPERATION_ACCOUNTID, (PrimitiveSchema) getSchema(BasicOntology.STRING), ObjectSchema.MANDATORY);
            cs.add(OPERATION_DATE, (PrimitiveSchema) getSchema(BasicOntology.DATE), ObjectSchema.MANDATORY);

            AgentActionSchema as = new AgentActionSchema(CREATE_ACCOUNT);
            add(as, CreateAccount.class);
            as.add(CREATE_ACCOUNT_NAME, (PrimitiveSchema) getSchema(BasicOntology.STRING), ObjectSchema.MANDATORY);

            add(as = new AgentActionSchema(MAKE_OPERATION), MakeOperation.class);
            as.add(MAKE_OPERATION_TYPE, (PrimitiveSchema) getSchema(BasicOntology.INTEGER), ObjectSchema.MANDATORY);
            as.add(MAKE_OPERATION_AMOUNT, (PrimitiveSchema) getSchema(BasicOntology.FLOAT), ObjectSchema.MANDATORY);
            as.add(MAKE_OPERATION_ACCOUNTID, (PrimitiveSchema) getSchema(BasicOntology.STRING), ObjectSchema.MANDATORY);

            add(as = new AgentActionSchema(INFORMATION), Information.class);
            as.add(INFORMATION_TYPE, (PrimitiveSchema) getSchema(BasicOntology.INTEGER), ObjectSchema.MANDATORY);
            as.add(INFORMATION_ACCOUNTID, (PrimitiveSchema) getSchema(BasicOntology.STRING), ObjectSchema.MANDATORY);
        }
        catch (OntologyException oe) {
            oe.printStackTrace();
        }
    }
}
