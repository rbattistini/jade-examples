package metajade.examples.bank.ontology;

import jade.content.AgentAction;


public class CreateAccount implements AgentAction {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
