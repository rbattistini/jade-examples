package metajade.examples.bank.utils;

public enum ErrorCodes {
    invalidAmount(1),
    accountNotFound(2),
    insufficientFunds(3);

    private final int value;
    ErrorCodes(int value) {
        this.value = value;
    }
    public int getValue() {
        return this.value;
    }

    public static ErrorCodes fromValue(int value) {
        for (ErrorCodes it : ErrorCodes.values()) {
            if (it.value == value) {
                return it;
            }
        }
        throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
}