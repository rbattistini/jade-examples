package metajade.examples.bank.utils;

public enum Command {
    new_account(1),
    deposit(2),
    balance(3),
    withdraw(4),
    operations(5);

    private final int value;

    public int getValue() {
        return this.value;
    }

    Command(int value) {
        this.value = value;
    }

    public static Command fromValue(int value) {
        for (Command it : Command.values()) {
            if (it.value == value) {
                return it;
            }
        }
        throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }

}
