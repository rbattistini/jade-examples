package metajade.examples.pingpong;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.domain.AMSService;
import jade.domain.FIPAAgentManagement.AMSAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.domain.FIPAException;
import jade.lang.acl.MessageTemplate;
import metajade.lib.Behaviours;
import metajade.lib.Performative;

import java.util.stream.Stream;

public class Pinger extends Agent {
    private final static String PING_MSG = "ping";

    @Override
    protected void setup() {
        final SearchConstraints searchConstraints = new SearchConstraints();
        searchConstraints.setMaxResults(-1L);
        try {
            Stream.of(AMSService.search(this, new AMSAgentDescription(), searchConstraints))
                    .map(AMSAgentDescription::getName)
                    .filter(aid -> aid.getLocalName().startsWith("ponger_"))
                    .findFirst()
                    .ifPresent(
                            pongAID -> addBehaviour(Behaviours.sequence(sendPing(pongAID),
                            receivePong(pongAID)))
                    );
        } catch (FIPAException e) {
            e.printStackTrace();
        }
    }

    private Behaviour receivePong(final AID pongAID) {
        return Behaviours.listen(
                MessageTemplate.MatchSender(pongAID),
                msg -> addBehaviour(sendPing(pongAID))
        );
    }

    private Behaviour sendPing(final AID pongAID) {
        return Behaviours.sendMessage(Performative.INFORM, PING_MSG, pongAID);
    }
}
