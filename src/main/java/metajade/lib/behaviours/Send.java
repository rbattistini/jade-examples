package metajade.lib.behaviours;

import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;

public abstract class Send extends OneShotBehaviour {
    @Override
    public void action() {
        getAgent().send(getMessage());
    }

    public abstract ACLMessage getMessage();
}
