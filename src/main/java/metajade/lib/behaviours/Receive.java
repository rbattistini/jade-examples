package metajade.lib.behaviours;

import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.util.Objects;

public class Receive extends SimpleBehaviour {

    private final MessageTemplate template;
    private ACLMessage msg;

    public Receive(MessageTemplate template) {
        this.template = Objects.requireNonNull(template);
    }

    @Override
    public void action() {
        msg = getAgent().receive(template);
        if(msg != null) {
            onMessageReceived(msg);
        } else {
            this.block();
        }
    }

    public void onMessageReceived(ACLMessage message) { /* to be overridden */ }

    @Override
    public boolean done() {
        return msg != null;
    }

    @Override
    public void handleRestartEvent() {
        super.handleRestartEvent();
        throw new Error("not implemented");
    }
}
