package metajade.lib.behaviours;

import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.util.Objects;

public class Listen extends CyclicBehaviour {

    private final MessageTemplate template;

    public Listen(MessageTemplate template) {
        this.template = Objects.requireNonNull(template);
    }

    @Override
    public void action() {
        final ACLMessage msg = getAgent().receive(template);
        if(msg != null) {
            onMessageReceived(msg);
        } else {
            block();
        }
    }

    public void onMessageReceived(ACLMessage message) { /* to be overridden */ }
}
