package metajade.lib;

import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.ParallelBehaviour;
import jade.core.behaviours.SequentialBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import metajade.lib.behaviours.*;

import java.util.Arrays;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class Behaviours {

    private Behaviours() {
        throw new IllegalStateException(
                String.format(
                        "Class %s cannot be instantiated: it can only contain static methods",
                        Behaviours.class
                )
        );
    }

    public static Behaviour atomic(Runnable action) {
        return new OneShotBehaviour() {
            @Override
            public void action() {
                action.run();
            }
        };
    }

    public static Behaviour sequence(Behaviour behaviour, Behaviour... behaviours) {
        final SequentialBehaviour sequenceBehaviour = new SequentialBehaviour();
        sequenceBehaviour.addSubBehaviour(behaviour);
        Arrays.stream(behaviours).forEach(sequenceBehaviour::addSubBehaviour);
        return sequenceBehaviour;
    }

    private static Behaviour parallel(int endCondition, Behaviour behaviour, Behaviour... behaviours) {
        final ParallelBehaviour parallelBehaviour = new ParallelBehaviour(endCondition);
        parallelBehaviour.addSubBehaviour(behaviour);
        Arrays.stream(behaviours).forEach(parallelBehaviour::addSubBehaviour);
        return parallelBehaviour;
    }

    public static Behaviour any(Behaviour behaviour, Behaviour... behaviours) {
        return parallel(ParallelBehaviour.WHEN_ANY, behaviour, behaviours);
    }

    public static Behaviour all(Behaviour behaviour, Behaviour... behaviours) {
        return parallel(ParallelBehaviour.WHEN_ALL, behaviour, behaviours);
    }

    public static Behaviour stop() {
        return new Stop();
    }

    public static Behaviour sendMessage(Supplier<ACLMessage> message) {
        return new Send() {
            @Override
            public ACLMessage getMessage() {
                return message.get();
            }
        };
    }

    public static Behaviour sendMessage(ACLMessage message) {
        return sendMessage(() -> message);
    }

    public static Behaviour sendMessage(Performative performative, Supplier<String> content, AID receiver, AID... otherReceivers) {
        return sendMessage(() -> Messages.message(performative, content,
                receiver, otherReceivers));
    }

    public static Behaviour sendMessage(Performative performative, String content, AID receiver, AID... otherReceivers) {
        return sendMessage(Messages.message(performative, content, receiver,
                otherReceivers));
    }

    public static Behaviour receiveMessage(Consumer<ACLMessage> messageReceived) {
        return receiveMessage(MessageTemplate.MatchAll(), messageReceived);
    }

    public static Behaviour receiveMessage(MessageTemplate template, Consumer<ACLMessage> messageReceived) {
        return new Receive(template) {
            @Override
            public void onMessageReceived(ACLMessage message) {
                messageReceived.accept(message);
            }
        };
    }

    public static Behaviour listen(Consumer<ACLMessage> messageReceived) {
        return listen(MessageTemplate.MatchAll(), messageReceived);
    }

    public static Behaviour listen(MessageTemplate template, Consumer<ACLMessage> messageReceived) {
        return new Listen(template) {
            @Override
            public void onMessageReceived(ACLMessage message) {
                messageReceived.accept(message);
            }
        };
    }

    public static Behaviour repeatWhile(Behaviour behaviour, Supplier<Boolean> condition) {
        return new DoWhile(behaviour) {
            @Override
            public boolean condition() {
                return condition.get();
            }
        };
    }

    public static Behaviour repeatForEver(Behaviour behaviour) {
        return new DoWhile(behaviour);
    }
}
