plugins {
    java
}

val platformName: String by project
val platformHost: String by project
val containerBaseName: String by project

repositories {
    mavenCentral()
    // maven("https://jade.tilab.com/maven/") // Stale certificates
}

dependencies {
    // implementation("com.tilab.jade:jade:4.5.0")
    implementation(fileTree("libs").also { it.include("**/*.jar") })
    testImplementation("junit:junit:4.13")
    testImplementation("org.junit.jupiter:junit-jupiter:5.8.1")
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

tasks.create<JavaExec>("startPlatform") {
    mainClass.set("jade.Boot")
    group = "run"
    sourceSets.main {
        classpath = runtimeClasspath
    }
    args("-gui", "-name", platformName, "-container-name", containerBaseName + "main", "-local-host", platformHost)
}

tasks.create<JavaExec>("startContainer") {
    mainClass.set("jade.Boot")
    group = "run"
    sourceSets.main {
        classpath = runtimeClasspath
    }
    args("-container", "-container-name", containerBaseName + System.currentTimeMillis(), "-host", platformHost)
    if (project.hasProperty("agents")) {
        val agents = project.property("agents").toString()
        if (agents.isNotBlank()) {
            args("-agents", agents)
        }
    }
}

