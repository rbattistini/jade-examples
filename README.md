# Jade-examples

Two simple multi-agent applications.

The first is an example of Ping Pong, similar to the one seen in https://gitlab.com/pika-lab/courses/ds/ay2122/lab-9.

The second shows a bank account management scenario, similar to the one seen in https://www.iro.umontreal.ca/~vaucher/Agents/Jade/Ontologies.htm.

To test the examples:

```sh
./gradlew check
```